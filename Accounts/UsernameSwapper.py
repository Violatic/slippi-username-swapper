import json
import glob
import sys


def let_user_pick(options):
    print("Please choose:")
    for idx, element in enumerate(options):
        print("{}) {}".format(idx+1, element))
    i = input("Enter number: ")
    try:
        if 0 < int(i) <= len(options):
            user_choice = int(i)
    except:
        pass
    return user_choice


with open('user.json') as json_file:
    main_file = json.load(json_file)

list_of_json = glob.glob("Accounts/*.json")
user_names = [x.split('.')[0] for x in list_of_json]
user_names = [x.split('\\')[1] for x in user_names]

if len(list_of_json) == 2:
    for x in list_of_json:
        with open(x) as json_file:
            parsed = json.load(json_file)
            if parsed['uid'] == main_file['uid']:
                print(x + " is the current user")
            else:
                print(x + " is not the current user")
                print("Swapping " + x + " to be user.json")
                with open('user.json', 'w') as outfile:
                    json.dump(parsed, outfile)
                sys.exit()
else:
    print("There are more than two accounts")
    account_choice = let_user_pick(user_names)
    print(user_names[account_choice-1] + " assigned to user.json")
    with open('user.json', 'w') as outfile:
        with open(list_of_json[account_choice-1], 'r') as file_replace:
            parsed = json.load(file_replace)
            json.dump(parsed, outfile)

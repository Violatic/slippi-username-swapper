# Slippi Username Swapper

A tool designed to allow people to easily switch their Slippi accounts for smurfing

## How it works

*NameSwapper.bat* is a batch file that executes *UsernameSwapper.py*

*UsernameSwapper.py* is a Python file that first checks if there are more than two accounts
if there are more than 2 accounts it brings up a user input so you can select the account
you wish to swap. If there are only two accounts it does a quick swap and informs you of
the swap.

## Requirements

1) Python 2.7 installed
2) Move the json for your smurfs to a subfolder within your Slippi folder called /Accounts

## How to Use

1) Download *NameSwapper.bat* and Accounts/*UsernameSwapper.py*
2) Place *NameSwapper.bat* in your FM Slippi folder
3) Create a directory in your FM Slippi folder called "Accounts"
4) Place *UsernameSwapper.py* inside the "Accounts" subfolder
5) Place all your smurf account json files inside the "Accounts" subfolder

Once you have gone through these steps you just need to click *NameSwapper.bat *

## FAQs

#### Why not just change your username by modifying User.json?
You can do this, and it will change the name you appear as when you connect.
However anybody who looks at the replay will still see your connection ID (i.e. Vio#974)
this it swaps out all of the details allowing you to be untraceable